package deptrai;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLiteHelper {
	private Connection connection;

	public SQLiteHelper(String path) {
		try {
			System.out.println(path);
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + path);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return;
		}
		System.out.println("Open " + path + " succesfully");
	}

	public int findIntAttribute(String findTable, String findField, String queryField, int queryValue) {
		if (connection == null)
			return -1;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT \"" + findField + "\" FROM \"" + findTable + "\" WHERE \"" + queryField + "\" = \""
					+ queryValue + "\"";
			System.out.println(query);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				return resultSet.getInt(findField);
			}
		} catch (Exception ex) {
			System.out.println("Exception : " + ex.getMessage());
		}
		return -1;
	}
}
