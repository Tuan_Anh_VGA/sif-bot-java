package deptrai;

import org.json.JSONObject;

public class JSONObjectHelper {
	@SuppressWarnings("unchecked")
	public static <T> T getOrDefault(JSONObject obj, String field, T def) {
		if (obj.has(field))
			return (T) obj.get(field);
		return def;
	}
}