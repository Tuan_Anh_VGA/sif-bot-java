package deptrai;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Formatter;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONArray;
import org.json.JSONObject;

public class HttpUtility {
	private static int nonce = 1;
	private static String authorizeKey = null;
	private static int user_id = 0;
	private static String login_key = null;
	private static int commandNumber = 2;
	/*
	 * HMAC_KEY = "liumangtuzi" for EN, "WytVirvyiab" for JP
	 * KLAB_HOST = "http://prod.en-lovelive.klabgames.net" for EN, "http://prod-jp.lovelive.ge.klabgames.net" for JP
	 * CLIENT_VERSION = 8.0.73 for EN, 20.7 for JP
	 * 
	 * */
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
	private static final String BUNDLE_VERSION = "4.1.2";
	private static final String CLIENT_VERSION = "9.0.24";
	private static final String HMAC_KEY = "liumangtuzi";
	private static final String KLAB_HOST = "http://prod.en-lovelive.klabgames.net";	

	public static String generateBoundary() {
		Random random = new Random();
		String boundary = "----------------------------";
		String table16 = "0123456789abcdef";
		for (int i = 0; i < 12; i++)
			boundary += (table16.charAt(random.nextInt(16)));
		return boundary;
	}

	public static void setAuthorizeKey(String key) {
		authorizeKey = key;
	}

	public static void setUserId(int i) {
		user_id = i;
	}

	public static void setLoginKey(String key) {
		login_key = key;
	}

	private static synchronized String corePost(String path, String data, long timeStamp) {
		String returnValue = "";
		HttpURLConnection connection = null;
		String host = KLAB_HOST;
		try {
			URL url = new URL(host + path);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("API-Model", "straightforward");
			if (authorizeKey == null)
				connection.setRequestProperty("Authorize",
						"consumerKey=lovelive_test&timeStamp=" + timeStamp + "version=1.1&nonce=" + nonce);
			else
				connection.setRequestProperty("Authorize", "consumerKey=lovelive_test&timeStamp=" + timeStamp
						+ "&version=1.1&token=" + authorizeKey + "&nonce=" + nonce);
			nonce++;
			connection.setRequestProperty("OS", "Android");
			connection.setRequestProperty("Bundle-Version", BUNDLE_VERSION);
			connection.setRequestProperty("Client-Version", CLIENT_VERSION);
			connection.setRequestProperty("Platform-Type", "2"); // android
			connection.setRequestProperty("Accept", "*/*");
			if (user_id != 0)
				connection.setRequestProperty("User-ID", String.valueOf(user_id));
			connection.setRequestProperty("Debug","1");
			connection.setRequestProperty("OS-Version","MI 4W Xiaomi MSM8974 6.0.1");
			connection.setRequestProperty("OS", "Android");
			connection.setRequestProperty("Platform-Type", "2");
			connection.setRequestProperty("Application-ID", "834030294");
			connection.setRequestProperty("Time-Zone", "Asia/Saigon");
			connection.setRequestProperty("Region", "392");
			
			connection.setUseCaches(false);
			connection.setDoOutput(true);

			if (data.length() > 0) {
				StringBuilder dataBuilder = new StringBuilder();
				String boundary = generateBoundary();
				dataBuilder.append("--" + boundary + "\r\n"); // first-line
				dataBuilder.append("Content-Disposition: form-data; name=\"request_data\"" + "\r\n"); // second-line
				dataBuilder.append("\r\n"); // third line
				dataBuilder.append(data + "\r\n");
				dataBuilder.append("--" + boundary + "--\r\n");
				
				connection.setRequestProperty("X-Message-Code", calculateHmac(data, HMAC_KEY));
				connection.setRequestProperty("Content-Length", String.valueOf(dataBuilder.toString().length()));
				connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
				
				data = dataBuilder.toString();
			}

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(data);
			wr.close();

			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
			}
			rd.close();
			returnValue = response.toString();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		} finally {
			if (connection != null)
				connection.disconnect();
		}
		return returnValue;
	}

	public static synchronized String klabPost(String path, JSONObject data) {
		long timeStamp = System.currentTimeMillis() / 1000;
		if (data.has("timeStamp"))
			data.put("timeStamp", String.valueOf(timeStamp));

		if (data.has("commandNum")) {
			StringBuilder commandNumBuilder = new StringBuilder();
			commandNumBuilder.append(login_key + ".");
			commandNumBuilder.append(timeStamp + ".");
			commandNumBuilder.append(commandNumber++);
			data.put("commandNum", commandNumBuilder.toString());
		}

		return corePost(path, data.toString(), timeStamp);
	}

	public static synchronized String klabPost(String path, JSONArray data) {
		long timeStamp = System.currentTimeMillis() / 1000;
		return corePost(path, data.toString(), timeStamp);
	}

	public static synchronized String klabPost(String path) {
		long timeStamp = System.currentTimeMillis() / 1000;
		return corePost(path, "", timeStamp);
	}

	public static String calculateHmac(String data, String key)
			throws SignatureException, InvalidKeyException, NoSuchAlgorithmException {
		SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
		Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
		mac.init(signingKey);
		return toHexString(mac.doFinal(data.getBytes()));
	}

	private static String toHexString(byte[] bytes) {
		Formatter formatter = new Formatter();

		for (byte b : bytes) {
			formatter.format("%02x", b);
		}
		String result = formatter.toString();
		formatter.close();
		return result;
	}
}
