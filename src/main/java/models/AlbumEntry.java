package models;

import org.json.JSONObject;

import deptrai.JSONObjectHelper;

public class AlbumEntry {
	private int unitId;
	private boolean rankMaxFlag;
	private boolean loveMaxFlag;
	private boolean rankLevelMaxFlag;
	private boolean allMaxFlag;
	private int highestLovePerUnit;
	private int totalLove;
	private int favoritePoint;
	
	public AlbumEntry(JSONObject obj){
		unitId = JSONObjectHelper.getOrDefault(obj, "unit_id", 0);
		rankMaxFlag = JSONObjectHelper.getOrDefault(obj, "rank_max_flag", false);
		loveMaxFlag = JSONObjectHelper.getOrDefault(obj, "love_max_flag", false);
		rankLevelMaxFlag = JSONObjectHelper.getOrDefault(obj, "rank_level_max_flag", false);
		allMaxFlag = JSONObjectHelper.getOrDefault(obj, "all_max_flag", false);
		highestLovePerUnit = JSONObjectHelper.getOrDefault(obj, "highest_love_per_unit", 0);
		totalLove = JSONObjectHelper.getOrDefault(obj, "total_love", 0);
		favoritePoint = JSONObjectHelper.getOrDefault(obj, "favorite_point", 0);
	}

	public int getUnitId() {
		return unitId;
	}

	public boolean isRankMaxFlag() {
		return rankMaxFlag;
	}

	public boolean isLoveMaxFlag() {
		return loveMaxFlag;
	}

	public boolean isRankLevelMaxFlag() {
		return rankLevelMaxFlag;
	}

	public boolean isAllMaxFlag() {
		return allMaxFlag;
	}

	public int getHighestLovePerUnit() {
		return highestLovePerUnit;
	}

	public int getTotalLove() {
		return totalLove;
	}

	public int getFavoritePoint() {
		return favoritePoint;
	}
}
