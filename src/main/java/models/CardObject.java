package models;

import org.json.JSONObject;

public class CardObject {
	private int unitOwningId;
	private int unitId;
	private int exp;
	private int nextExp;
	private int level;
	private int maxLevel;
	private int rank;
	private int maxRank;
	private int love;
	private int maxLove;
	private int unitSkillLevel;
	private int unitSkillExp;
	private int maxHp;
	private int unitRemovableSkillCapacity;
	private boolean isFavorite;
	private int displayRank;
	private boolean isRankMax;
	private boolean isLoveMax;
	private boolean isLevelMax;
	private boolean isSkillLevelMax;
	private boolean isRemovableSkillCapacityMax;

	public CardObject(JSONObject source) {
		unitOwningId = source.getInt("unit_owning_user_id");
		unitId = source.getInt("unit_id");
		exp = source.getInt("exp");
		nextExp = source.getInt("next_exp");
		level = source.getInt("level");
		maxLevel = source.getInt("max_level");
		rank = source.getInt("rank");
		maxRank = source.getInt("max_rank");
		love = source.getInt("love");
		maxLove = source.getInt("max_love");
		unitSkillLevel = source.getInt("unit_skill_level");
		unitSkillExp = source.getInt("unit_skill_exp");
		maxHp = source.getInt("max_hp");
		unitRemovableSkillCapacity = source.getInt("unit_removable_skill_capacity");
		isFavorite = source.getBoolean("favorite_flag");
		displayRank = source.getInt("display_rank");
		isRankMax = source.getBoolean("is_rank_max");
		isLoveMax = source.getBoolean("is_love_max");
		isLevelMax = source.getBoolean("is_level_max");
		isSkillLevelMax = source.getBoolean("is_skill_level_max");
		isRemovableSkillCapacityMax = source.getBoolean("is_removable_skill_capacity_max");
	}

	public int getUnitOwningId() {
		return unitOwningId;
	}

	public int getUnitId() {
		return unitId;
	}

	public int getExp() {
		return exp;
	}

	public int getNextExp() {
		return nextExp;
	}

	public int getLevel() {
		return level;
	}

	public int getMaxLevel() {
		return maxLevel;
	}

	public int getRank() {
		return rank;
	}

	public int getMaxRank() {
		return maxRank;
	}

	public int getLove() {
		return love;
	}

	public int getMaxLove() {
		return maxLove;
	}

	public int getUnitSkillLevel() {
		return unitSkillLevel;
	}

	public int getUnitSkillExp() {
		return unitSkillExp;
	}

	public int getMaxHp() {
		return maxHp;
	}

	public int getUnitRemovableSkillCapacity() {
		return unitRemovableSkillCapacity;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public int getDisplayRank() {
		return displayRank;
	}

	public boolean isRankMax() {
		return isRankMax;
	}

	public boolean isLoveMax() {
		return isLoveMax;
	}

	public boolean isLevelMax() {
		return isLevelMax;
	}

	public boolean isSkillLevelMax() {
		return isSkillLevelMax;
	}

	public boolean isRemovableSkillCapacityMax() {
		return isRemovableSkillCapacityMax;
	}
}
