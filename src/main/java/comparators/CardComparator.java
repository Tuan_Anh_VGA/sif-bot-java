package comparators;

import java.util.Comparator;

import models.CardObject;

public class CardComparator implements Comparator<CardObject> {

	@Override
	public int compare(CardObject o1, CardObject o2) {
		// compare by rank first
		int rank1, rank2;
		rank1 = o1.getRank() > 2 ? 2 : o1.getRank();
		rank2 = o2.getRank() > 2 ? 2 : o2.getRank();
		if (rank1 == 1 && rank2 == 2)
			return 1;
		if (rank1 == 2 && rank2 == 1)
			return -1;

		// then compare by love to max
		int loveToMax1, loveToMax2;
		loveToMax1 = o1.getMaxLove() - o1.getLove();
		loveToMax2 = o2.getMaxLove() - o2.getLove();
		return loveToMax2 - loveToMax1;
	}
}