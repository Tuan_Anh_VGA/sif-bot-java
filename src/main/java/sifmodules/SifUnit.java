package sifmodules;

import org.json.JSONArray;
import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifUnit {
	public static String practiceWithSupport(int ownId,int supporterId,int amount){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"unit\",\"unit_owning_user_ids\":[],\"action\":\"merge\",\"timeStamp\":0,\"base_owning_unit_user_id\":199918753,\"mgd\":1,\"unit_support_list\":[{\"amount\":1,\"unit_id\":383}],\"commandNum\":\"\"}");
		dataTemplate.put("base_owning_unit_user_id", ownId);
		JSONArray supportObj = dataTemplate.getJSONArray("unit_support_list");
		supportObj.getJSONObject(0).put("unit_id", supporterId);
		supportObj.getJSONObject(0).put("amount",amount);
		dataTemplate.put("unit_support_list", supportObj);
		
		return HttpUtility.klabPost("/main.php/unit/merge",dataTemplate);
	}
	
	public static String idolizeWithSeal(int ownId,int exchangePointId){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"unit\",\"exchange_point_id\":1,\"action\":\"exchangePointRankUp\",\"timeStamp\":0,\"base_owning_unit_user_id\":1,\"mgd\":1,\"commandNum\":\"\"}");
		dataTemplate.put("base_owning_unit_user_id", ownId);
		dataTemplate.put("exchange_point_id", exchangePointId);
		return HttpUtility.klabPost("/main.php/unit/exchangePointRankUp",dataTemplate);
	}
	
	public static String idolize(int baseOwnId,int practiceOwnId){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"unit\",\"unit_owning_user_ids\":[],\"action\":\"rankUp\",\"timeStamp\":0,\"base_owning_unit_user_id\":1,\"mgd\":2,\"commandNum\":\"\"}");
		JSONArray practiceArray = new JSONArray();
		practiceArray.put(practiceOwnId);
		dataTemplate.put("unit_owning_user_ids", practiceArray);
		dataTemplate.put("base_owning_unit_user_id", baseOwnId);
		return HttpUtility.klabPost("/main.php/unit/rankUp",dataTemplate);
	}
}
