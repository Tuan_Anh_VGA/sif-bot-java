package sifmodules;

import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifFestival {
	public static String getDeckList() {
		JSONObject dataTemplate = new JSONObject("{\"module\":\"festival\",\"mgd\":1,\"action\":\"deckList\",\"timeStamp\":0,\"live_count\":1,\"difficulty\":1,\"event_id\":74,\"commandNum\":\"\"}");
		return HttpUtility.klabPost("/main.php/festival/deckList", dataTemplate);
	}

	public static String reroll(){
		JSONObject dataTemplate= new JSONObject("{\"module\":\"festival\",\"action\":\"updateLiveList\",\"timeStamp\":0,\"mgd\":1,\"event_id\":74,\"commandNum\":\"\"}");
		return HttpUtility.klabPost("/main.php/festival/updateLiveList",dataTemplate);
	}
}
