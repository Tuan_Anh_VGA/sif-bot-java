package sifmodules;

import org.json.JSONArray;
import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifRune {
	public static String getRuneInfo() {
		JSONArray data = new JSONArray();
		JSONObject runeModule = new JSONObject(
				"{\"module\":\"unit\",\"action\":\"removableSkillInfo\",\"timeStamp\":1481041785}");
		runeModule.put("timeStamp", System.currentTimeMillis() / 1000);
		data.put(runeModule);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2", data);
	}

	public static String keepRuneAmount(int amount) {
		JSONObject result = null, data = null;
		result = new JSONObject(getRuneInfo());
		if (result.has("response_data"))
			data = result.getJSONArray("response_data").getJSONObject(0);

		JSONArray list = data.getJSONObject("result").getJSONArray("owning_info");
		JSONArray removeList = new JSONArray();

		for (int i = 0; i < list.length(); i++) {
			JSONObject listItem = list.getJSONObject(i);
			if (listItem.getInt("total_amount") > amount) {
				JSONObject item = new JSONObject();
				item.put("unit_removable_skill_id", listItem.getInt("unit_removable_skill_id"));
				item.put("amount", listItem.getInt("total_amount") - amount);
				removeList.put(item);
			}
		}

		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"unit\",\"selling_list\":[],\"action\":\"removableSkillSell\",\"timeStamp\":0,\"mgd\":1,\"commandNum\":\"\"}");
		dataTemplate.put("selling_list", removeList);

		System.out.println("Selling skills...");
		String retVal = HttpUtility.klabPost("/main.php/unit/removableSkillSell?language=eng&store=2", dataTemplate);
		System.out.println("Sold");
		return retVal;
	}
}