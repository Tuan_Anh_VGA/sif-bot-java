package sifmodules;

import org.json.JSONArray;
import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifSupporter {
	public static String getSupporterInfo() {
		System.out.println("Collecting supporters data...");
		JSONArray data = new JSONArray();
		JSONObject supporterModule = new JSONObject(
				"{\"module\":\"unit\",\"action\":\"supporterAll\",\"timeStamp\":0}");
		supporterModule.put("timeStamp", System.currentTimeMillis() / 1000);
		data.put(supporterModule);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2", data);
	}

	public static String keepSupporterInfo(int amount) {
		JSONObject result = null, data = null;
		result = new JSONObject(getSupporterInfo());
		if (result.has("response_data"))
			data = result.getJSONArray("response_data").getJSONObject(0);

		JSONArray list = data.getJSONObject("result").getJSONArray("unit_support_list");
		JSONArray removeList = new JSONArray();

		for (int i = 0; i < list.length(); i++) {
			JSONObject listItem = list.getJSONObject(i);
			int id = listItem.getInt("unit_id");
			if (id > 382 && id < 387)
				continue;
			if (listItem.getInt("amount") > amount) {
				JSONObject item = new JSONObject();
				item.put("unit_id", listItem.getInt("unit_id"));
				item.put("amount", listItem.getInt("amount") - amount);
				removeList.put(item);
			}
		}

		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"unit\",\"action\":\"sale\",\"timeStamp\":0,\"mgd\":1,\"unit_support_list\":[],\"unit_owning_user_id\":[],\"commandNum\":\"\"}");
		dataTemplate.put("unit_support_list", removeList);

		System.out.println("Selling supporters...");
		String retVal = HttpUtility.klabPost("/main.php/unit/sale", dataTemplate);
		System.out.println("Sold");
		return retVal;
	}
}
