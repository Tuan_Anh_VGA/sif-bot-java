package sifmodules;

import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifAccount {
	public static String authenticate() {
		String result = HttpUtility.klabPost("/main.php/login/authkey");
		JSONObject object = new JSONObject(result);
		JSONObject data = null;
		if (object.has("response_data"))
			data = object.getJSONObject("response_data");

		if (data.has("authorize_token")) {
			HttpUtility.setAuthorizeKey(data.getString("authorize_token"));
			return data.getString("authorize_token");
		}
		return "";
	}

	public static String login(String key, String pass) {
		JSONObject data = new JSONObject();
		data.put("country_code", "");
		data.put("login_key", key);
		data.put("login_passwd", pass);
		data.put("devtoken", "APA91bE9dl530G16I717ohjeUZ29KSHITXbdatnB4c_UUiG1qO6xTVgq7Yo5kl8Rva_3i_J8_yHUQIfDY-ef0WOf31T3-NB6SyXDEmVZ3of5aVdfifXPfr6Y8MXrRQUnSHULxKyhO0od");
		String result = HttpUtility.klabPost("/main.php/login/login", data);
		JSONObject resultObj = new JSONObject(result);
		if (resultObj.has("response_data"))
			resultObj = resultObj.getJSONObject("response_data");

		if (resultObj.has("authorize_token"))
			HttpUtility.setAuthorizeKey(resultObj.getString("authorize_token"));

		if (resultObj.has("user_id"))
			HttpUtility.setUserId(resultObj.getInt("user_id"));

		HttpUtility.setLoginKey(key);
		return result;
	}

	public static String getUserInfo(){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"user\",\"action\":\"userInfo\",\"timeStamp\":0,\"mgd\":1,\"commandNum\":\"\"}");
		return HttpUtility.klabPost("/main.php/user/userInfo?language=eng&store=2",dataTemplate);
	}
	
	public static String recoverLP(){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"common\",\"action\":\"recoveryEnergy\",\"timeStamp\":0,\"mgd\":1,\"commandNum\":\"\"}");
		return HttpUtility.klabPost("/main.php/common/recoveryEnergy?language=eng&store=2",dataTemplate);
	}
}
