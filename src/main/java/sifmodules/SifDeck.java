package sifmodules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import comparators.CardComparator;
import deptrai.HttpUtility;
import deptrai.SQLiteHelper;
import models.AlbumEntry;
import models.CardObject;

public class SifDeck {
	private static final int ATTRIBUTE_SMILE = 1;
	private static final int ATTRIBUTE_PURE = 2;
	private static final int ATTRIBUTE_COOL = 3;

	private static final int PRACTICE_SMILE_SUPPORTER_ID = 28;
	private static final int PRACTICE_PURE_SUPPORTER_ID = 29;
	private static final int PRACTICE_COOL_SUPPORTER_ID = 30;

	private static HashMap<Integer, AlbumEntry> albumMap;
	private static List<Integer> reserveList;
	static {
		albumMap = new HashMap<Integer, AlbumEntry>();
		reserveList = new ArrayList<>();
	}

	public static String getAllUnitInfo() {
		JSONObject dataTemplate = new JSONObject("{\"module\":\"unit\",\"action\":\"unitAll\",\"timeStamp\":0}");
		JSONArray data = new JSONArray();
		data.put(dataTemplate);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2", data);
	}

	public static String getDeckInfo() {
		JSONObject dataTemplate = new JSONObject("{\"module\":\"unit\",\"action\":\"deckInfo\",\"timeStamp\":0}");
		JSONArray data = new JSONArray();
		data.put(dataTemplate);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2", data);
	}

	public static String getAlbumInfo() {
		JSONObject dataTemplate = new JSONObject("{\"module\":\"album\",\"action\":\"albumAll\",\"timeStamp\":0}");
		JSONArray data = new JSONArray();
		data.put(dataTemplate);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2", data);
	}

	public static void updateAlbumInfo() {
		albumMap.clear();
		JSONObject resultObj = new JSONObject(getAlbumInfo());
		JSONArray resultArray = resultObj.getJSONArray("response_data").getJSONObject(0).getJSONArray("result");
		for (int i = 0; i < resultArray.length(); i++) {
			JSONObject element = resultArray.getJSONObject(i);
			AlbumEntry entry = new AlbumEntry(element);
			albumMap.put(entry.getUnitId(), entry);
		}
	}

	public static void updateReserveList() {
		reserveList.clear();
		// get partner object
		JSONObject resultObj = new JSONObject(SifModule.getModuleInfo("user", "getNavi"));
		JSONObject naviObj = resultObj.getJSONArray("response_data").getJSONObject(0).getJSONObject("result")
				.getJSONObject("user");

		if (naviObj.has("unit_owning_user_id")) {
			reserveList.add(naviObj.getInt("unit_owning_user_id"));
		}

		resultObj = new JSONObject(SifDeck.getDeckInfo());
		JSONArray deckArray = resultObj.getJSONArray("response_data").getJSONObject(0).getJSONArray("result");
		for (int i = 0; i < deckArray.length(); i++) {
			JSONObject deck = deckArray.getJSONObject(i);
			if (deck.has("main_flag") == false || deck.getBoolean("main_flag") == false) {
				continue;
			}
			JSONArray unitArray = deck.getJSONArray("unit_owning_user_ids");
			for (int j = 0; j < unitArray.length(); j++) {
				int id = unitArray.getJSONObject(j).getInt("unit_owning_user_id");
				reserveList.add(id);
			}
		}
	}

	// idolized SR first
	// rarity 1 - normal,2 - rare, 3 - super rare
	public static void autoPractice(int rarity) {
		JSONArray deckArray = getAllUnitArray();
		int[] rarityArray = { 0, 40, 60, 80 };
		int[] practiceQuantity = { 0, 2, 4, 7 };

		for (int i = 0; i < deckArray.length(); i++) {
			CardObject cardObj = new CardObject(deckArray.getJSONObject(i));
			if (cardObj.getRank() == 1 || cardObj.isLevelMax() || (cardObj.isLoveMax() == false)
					|| (cardObj.isRankMax() == false))
				continue;

			if (cardObj.getMaxLevel() > rarityArray[rarity])
				continue;

			int attribute = getAttributeByUnitId(cardObj.getUnitId());
			switch (attribute) {
			case ATTRIBUTE_SMILE:
				SifUnit.practiceWithSupport(cardObj.getUnitOwningId(), PRACTICE_SMILE_SUPPORTER_ID,
						practiceQuantity[rarity]);
				break;
			case ATTRIBUTE_PURE:
				SifUnit.practiceWithSupport(cardObj.getUnitOwningId(), PRACTICE_PURE_SUPPORTER_ID,
						practiceQuantity[rarity]);
				break;
			case ATTRIBUTE_COOL:
				SifUnit.practiceWithSupport(cardObj.getUnitOwningId(), PRACTICE_COOL_SUPPORTER_ID,
						practiceQuantity[rarity]);
				break;

			default:
				break;
			}

			System.out.println("Practiced unit id : " + cardObj.getUnitOwningId());
		}
	}

	// SR only
	public static void autoIdolizeBySeal() {
		JSONArray deckArray = getAllUnitArray();
		updateAlbumInfo();

		for (int i = 0; i < deckArray.length(); i++) {
			CardObject cardObj = new CardObject(deckArray.getJSONObject(i));
			if ((cardObj.getRank() != 1) || cardObj.getMaxLevel() < 40)
				continue;
			if (albumMap.get(cardObj.getUnitId()).isRankMaxFlag())
				continue;

			SifUnit.idolizeWithSeal(cardObj.getUnitOwningId(), 2);
			System.out.println("Idolized unit id : " + cardObj.getUnitOwningId());
		}
	}

	// remove cards which have rank 1 and idolized earlier
	public static String removeTrashCards(boolean removeIdolized) {
		JSONArray deckArray = getAllUnitArray();
		JSONArray removeArray = new JSONArray();
		updateAlbumInfo();
		updateReserveList();
		for (int i = 0; i < deckArray.length(); i++) {
			CardObject cardObj = new CardObject(deckArray.getJSONObject(i));
			if ((cardObj.getRank() != 1 && removeIdolized == false) || (cardObj.getMaxLevel() > 80))
				continue;
			AlbumEntry entry = albumMap.getOrDefault(cardObj.getUnitId(), null);
			if (entry == null || (!entry.isAllMaxFlag()))
				continue;
			if (reserveList.contains(cardObj.getUnitOwningId()))
				continue;
			removeArray.put(cardObj.getUnitOwningId());
			System.out.println("Added card " + cardObj.getUnitOwningId() + " to removeList");
		}
		if (removeArray.length() > 0)
			return removeCards(removeArray);
		return deckArray.toString();
	}

	public static String removeTrashCards() {
		return removeTrashCards(false);
	}

	public static String removeCards(JSONArray removeList) {
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"unit\",\"action\":\"sale\",\"timeStamp\":0,\"mgd\":1,\"unit_support_list\":[],\"unit_owning_user_id\":[],\"commandNum\":\"\"}");
		dataTemplate.put("unit_owning_user_id", removeList);
		return HttpUtility.klabPost("/main.php/unit/sale", dataTemplate);
	}

	public static int getAttributeByUnitId(int id) {
		SQLiteHelper helper = new SQLiteHelper("unit.db");
		return helper.findIntAttribute("unit_m", "attribute_id", "unit_id", id);
	}

	public static String arrangeBondTrainTeam() {
		JSONArray resultArray = getAllUnitArray();
		List<CardObject> cards = new ArrayList<>();
		for (int i = 0; i < resultArray.length(); i++) {
			cards.add(new CardObject(resultArray.getJSONObject(i)));
		}

		cards.sort(new CardComparator());
		List<Integer> elements = new ArrayList<>();
		for (int i = 0; i < 9; i++) {
			elements.add(cards.get(i).getUnitOwningId());
		}
		return editDeck(1, true, elements);
	}

	public static String editDeck(int deckId, boolean isMain, List<Integer> elements) {
		JSONObject deck = new JSONObject(
				"{\"unit_deck_detail\":[],\"unit_deck_id\":1,\"main_flag\":1,\"deck_name\":\"Team A\"}");
		deck.put("unit_deck_id", deckId);
		deck.put("main_flag", isMain ? 1 : 0);
		JSONArray deckElements = new JSONArray();
		for (int i = 0; i < elements.size(); i++) {
			JSONObject elementObject = new JSONObject();
			elementObject.put("position", i + 1);
			elementObject.put("unit_owning_user_id", elements.get(i));
			deckElements.put(elementObject);
		}
		deck.put("unit_deck_detail", deckElements);
		JSONArray decks = new JSONArray();
		decks.put(deck);
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"unit\",\"unit_deck_list\":[],\"action\":\"deck\",\"mgd\":2}");
		dataTemplate.put("unit_deck_list", decks);
		System.out.println(dataTemplate.toString());
		return HttpUtility.klabPost("/main.php/unit/deck", dataTemplate);
	}

	public static JSONArray getAllUnitArray() {
		JSONObject result = new JSONObject(getAllUnitInfo());
		return result.getJSONArray("response_data").getJSONObject(0).getJSONArray("result");
	}

	public static void autoIdolizeNCard() {
		updateAlbumInfo();
		updateReserveList();
		JSONArray resultArray = getAllUnitArray();
		Map<Integer, ArrayList<CardObject>> unitMap = new HashMap<>();

		// add to unidolized unitMap
		for (int i = 0; i < resultArray.length(); i++) {
			CardObject co = new CardObject(resultArray.getJSONObject(i));

			// dodge idolized unit
			if (albumMap.get(co.getUnitId()).isRankMaxFlag())
				continue;

			// dodge R or above rank
			if (co.getMaxLevel() > 30)
				continue;

			ArrayList<CardObject> unitArray = unitMap.getOrDefault(co.getUnitId(), null);
			if (unitArray == null) {
				unitArray = new ArrayList<>();
				unitMap.put(co.getUnitId(), unitArray);
			}

			unitArray.add(co);
		}

		// iterate through unidolized unit
		Iterator<Entry<Integer, ArrayList<CardObject>>> it = unitMap.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, ArrayList<CardObject>> pair = it.next();

			// not enough card
			if (pair.getValue().size() < 2)
				continue;

			int baseOwnId = 0, practiceOwnId = 0;
			// select a not-reserved card
			int index = 0;
			ArrayList<CardObject> cards = pair.getValue();
			while (index < cards.size() && reserveList.contains(cards.get(index)))
				index++;

			if (index >= cards.size())
				continue;

			practiceOwnId = cards.get(index).getUnitOwningId();
			if (index == 0) {
				baseOwnId = cards.get(1).getUnitOwningId();
			} else {
				baseOwnId = cards.get(0).getUnitOwningId();
			}

			SifUnit.idolize(baseOwnId, practiceOwnId);
			System.out.println("Idolized unit id : " + cards.get(0).getUnitId());
		}
	}
}
