package sifmodules;

import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifSecretBox {
	public static String scoutMuseFriendPoint() {
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"secretbox\",\"action\":\"multi\",\"mgd\":1,\"term_count\":null,\"secret_box_id\":1,\"count\":10,\"commandNum\":\"\",\"cost_priority\":2,\"timeStamp\":0}");
		return HttpUtility.klabPost("/main.php/secretbox/multi", dataTemplate);
	}
	
	public static String scoutAquorsFriendPoint() {
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"secretbox\",\"action\":\"multi\",\"mgd\":1,\"term_count\":null,\"secret_box_id\":61,\"count\":10,\"commandNum\":\"\",\"cost_priority\":2,\"timeStamp\":0}");
		return HttpUtility.klabPost("/main.php/secretbox/multi", dataTemplate);
	}
	
	public static String scoutBlueTicket(){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"secretbox\",\"mgd\":1,\"action\":\"pon\",\"timeStamp\":0,\"term_count\":null,\"cost_priority\":1,\"secret_box_id\":22,\"commandNum\":\"\"}");
		return HttpUtility.klabPost("/main.php/secretbox/pon",dataTemplate);
	}
}
