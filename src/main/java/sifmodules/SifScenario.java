package sifmodules;

import org.json.JSONArray;
import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifScenario {
	public static String startSubScenario(int id){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"subscenario\",\"action\":\"startup\",\"timeStamp\":0,\"subscenario_id\":788,\"mgd\":1,\"commandNum\":\"\"}");
		dataTemplate.put("subscenario_id", id);
		return HttpUtility.klabPost("/main.php/subscenario/startup",dataTemplate);
	}
	
	public static String getSubScenarioReward(int id){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"subscenario\",\"action\":\"reward\",\"timeStamp\":0,\"subscenario_id\":788,\"mgd\":1,\"commandNum\":\"\"}");
		dataTemplate.put("subscenario_id", id);
		return HttpUtility.klabPost("/main.php/subscenario/reward",dataTemplate);
	}
	
	public static String getSubScenarioStatus(){
		JSONObject dataTemplate = new JSONObject("{\"module\":\"subscenario\",\"action\":\"subscenarioStatus\",\"timeStamp\":0}");
		dataTemplate.put("timeStamp", System.currentTimeMillis()/1000);
		JSONArray data = new JSONArray();
		data.put(dataTemplate);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2",data);
	}
	
	public static String clearAllScenario(){
		JSONObject data = new JSONObject(getSubScenarioStatus());
		data = data.getJSONArray("response_data").getJSONObject(0);
		JSONArray scenarios = data.getJSONObject("result").getJSONArray("subscenario_status_list");
		for (int i = 0;i<scenarios.length();i++) {
			JSONObject scenario = scenarios.getJSONObject(i);
			if (scenario.getInt("status") == 1){
				int scenarioId = scenario.getInt("subscenario_id");
				startSubScenario(scenarioId);
				getSubScenarioReward(scenarioId);
				System.out.println("Story " + scenarioId + " cleared.");
			}
		}
		return data.toString();
	}
}
