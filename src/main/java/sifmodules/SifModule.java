package sifmodules;

import org.json.JSONArray;
import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifModule {
	public static String getModuleInfo(String module, String action) {
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"" + module + "\",\"action\":\"" + action + "\",\"timeStamp\":0}");
		dataTemplate.put("timeStamp", System.currentTimeMillis() / 1000);
		JSONArray data = new JSONArray();
		data.put(dataTemplate);
		return HttpUtility.klabPost("/main.php/api?language=eng&store=2", data);
	}
}
