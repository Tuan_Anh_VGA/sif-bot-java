package sifmodules;

import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import deptrai.HttpUtility;

public class SifLive {
	public static String getPartyList(int liveDifficultyId) {
		System.out.println("Choosing party user..");
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"live\",\"action\":\"partyList\",\"timeStamp\":0,\"mgd\":1,\"live_difficulty_id\":\"1\",\"commandNum\":\"\"}");
		dataTemplate.put("live_difficulty_id", liveDifficultyId);
		//comment below line for EN
		dataTemplate.put("lp_factor", 1);
		return HttpUtility.klabPost("/main.php/live/partyList?language=eng&store=2", dataTemplate);
	}

	public static String playLive(int partyUserId, int liveDifficultyId) {
		System.out.println("Choosed user id : " + partyUserId);
		System.out.println("Playing...");
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"live\",\"party_user_id\":0,\"action\":\"play\",\"timeStamp\":0,\"mgd\":1,\"unit_deck_id\":1,\"live_difficulty_id\":1,\"commandNum\":\"\"}");
		dataTemplate.put("party_user_id", partyUserId);
		dataTemplate.put("live_difficulty_id", liveDifficultyId);
		dataTemplate.put("unit_deck_id",1);
		//comment below line for EN
		dataTemplate.put("lp_factor", 1);
		return HttpUtility.klabPost("/main.php/live/play?language=eng&store=2", dataTemplate);
	}

	public static String getLiveReward(int liveDifficultyId, int dedicatedScore, int dedicatedCombo) {
		JSONObject dataTemplate = new JSONObject(
				"{\"module\":\"live\",\"action\":\"reward\",\"good_cnt\":0,\"miss_cnt\":95,\"great_cnt\":0,\"commandNum\":\"\",\"love_cnt\":50000,\"max_combo\":666,\"score_smile\":999999999,\"perfect_cnt\":0,\"bad_cnt\":0,\"event_point\":0,\"live_difficulty_id\":1,\"timeStamp\":0,\"mgd\":1,\"score_cute\":0,\"score_cool\":0,\"event_id\":null}");
		dataTemplate.put("live_difficulty_id", liveDifficultyId);
		dataTemplate.put("score_smile", dedicatedScore);
		dataTemplate.put("max_combo", dedicatedCombo);
		return HttpUtility.klabPost("/main.php/live/reward?language=eng&store=2", dataTemplate);
	}

	// use coin-limit = -1 to disable coin-using
	// loop = false also disable coin-using
	public static void doLiveBot(int liveDifficultyId, int dedicatedScore, int dedicatedCombo, boolean loop,
			int coinLimit) {
		Random random = new Random();
		JSONObject resultObj = null, dataObj = null;

		do {
			// pick party phase
			resultObj = new JSONObject(getPartyList(liveDifficultyId));
			if (resultObj.has("response_data"))
				dataObj = resultObj.getJSONObject("response_data");

			if (dataObj.has("error_code")) {
				if (coinLimit == -1) {
					System.out.println("Error code " + dataObj.getInt("error_code") + " encountered, exit.. ");
					return;
				}else{
					JSONObject userInfo = new JSONObject(SifAccount.getUserInfo());
					int sns_coin = userInfo.getJSONObject("response_data")
							.getJSONObject("user")
							.getInt("sns_coin");
					if (coinLimit < 0 || sns_coin <= coinLimit){
						System.out.println("Coin limit exceeded");
						return;
					}
					SifAccount.recoverLP();
					
					//repick party
					resultObj = new JSONObject(getPartyList(liveDifficultyId));
					if (resultObj.has("response_data"))
						dataObj = resultObj.getJSONObject("response_data");
				}
			}

			JSONArray partyList = dataObj.getJSONArray("party_list");
			JSONObject randomUser = partyList.getJSONObject(random.nextInt(partyList.length()));
			int partyId = randomUser.getJSONObject("user_info").getInt("user_id");

			// play phase
			playLive(partyId, liveDifficultyId);

			// get reward phase
			getLiveReward(liveDifficultyId, dedicatedScore, dedicatedCombo);
			System.out.println("Done playing.");

			// remove removable skills
			//SifRune.keepRuneAmount(150);

			// remove supporters
			//SifSupporter.keepSupporterInfo(650);
			
			// remove trashes
			SifDeck.removeTrashCards(true);
		} while (loop);
	}
}
